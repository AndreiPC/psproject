# PSproject
The project will contain a standard MVC configuration with Clients, Products, Categories and Staff models and the coresponding Service Interfaces, Service Implementations, Repositories and Controllers.
The architecture has the controoler as the logic which uses an Service that is using a repository to acces the DataBase.

So far:
The client and staff implemented with full basic data base connection and querry.
The foreign key in the Product-Category pair is not working yet.


## Test and Deploy



***
## Name
Online Shop for furniture

## Description
The shop represents an android application with client and staff interfaces. Clients are basic users that can visualise the products, prices, add products to cart and make orders. Staff are shop maintainers that actually restock, modify products, add new products etc.

The main server will be hosted on a PC with the logic in java spring-boot framework and the data will be stored in a MySQLServer DB.

## Badges


## Visuals


## Installation


## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
